function counterFactory(counter) {
    let increment=function(){
        return counter+1;
    }
    let decrement=function(){
        console.log(counter)
        return counter-1;
    }
    let object={increment,decrement};
    return object;

}

module.exports={counterFactory};

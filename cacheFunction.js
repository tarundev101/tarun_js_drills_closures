function cacheFunction(cb) {
//    here, if we see the occurance of repeated element then we should not call it and rather that return the cache (object). 
    if (typeof cb === 'string'){
        cb = cb.split(' ');
    }
    let res = function(){
        let cache={};
        var count='true';
        for (let i = 0;i < cb.length;i++){
            if (!cache.hasOwnProperty(cb[i])){
                 cache[cb[i]] = (cb[i]);
                // return cb;

            }
            else{
                count = 'false';
                return cache;
            }
        }
        return cache;

    }
    return res;
}
module.exports = {cacheFunction}
